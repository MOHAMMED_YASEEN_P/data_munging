"""
Data Munging Project.

objectives:

output the day number (column one) with the smallest temperature spread.

print the name of the team with the smallest difference in
‘for’ and ‘against’ goals.
"""
from configparser import ConfigParser
from py_files import weather
from py_files import football
from py_files.helper_class import DataExtractor, DataAnalyzer
CONFIG = ConfigParser()
CONFIG.read("config.ini")

def main():
    """main function"""
    extracted_weather_data = DataExtractor(CONFIG['file']['FILE1'])
    extracted_weather_data.extract_data()
    weather_data_analyzer = weather.WeatherAnalyzer()
    weather_data_analyzer.filter_weather_data(extracted_weather_data, [0, 1, 2], [0, 1,\
        extracted_weather_data.length-1, extracted_weather_data.length-2])
    weather_data_analyzer.get_minimum_difference()
    weather_data_analyzer.get_result()


    extracted_football_data = DataExtractor(CONFIG['file']['FILE2'])
    extracted_football_data.extract_data()
    football_data_analayzer = football.FootballAnalyzer()
    football_data_analayzer.filter_football_data(extracted_football_data,[1, 6, 8],\
        [0, extracted_football_data.length-1])
    football_data_analayzer.get_minimum_difference()
    football_data_analayzer.get_result()


if __name__ == "__main__":
    main()
