"""Football module"""
from py_files import helper_class

class FootballAnalyzer(helper_class.DataAnalyzer):
    """
    football class wchich inherits ExtractData class and DataAnalyzer \
        class of heper module
    """
    def __init__(self):
        self.minimum_difference = None
        self.output = None
    def filter_football_data(self,extracted_data, column_to_be_extracted, row_to_exclude):
        """
        filter and removes all the unnecessary rows and columns of \
            football object.

        :param self : Football object.
        :type self Football object.
        :param column_to_be_extracted : column in which requried \
            data is present.
        :type column_to_be_extracted : list
        :param row_to_exclude : row that is not needed.
        :type row_to_exclude : list
        """
        self.filtered_data = []
        self.filter_data(extracted_data,column_to_be_extracted)
        filtered_data = []
        for row_number,row in enumerate(self.data):
                self.filtered_data.append([row[0],int(row[1]),int(row[2])])

    def get_result(self):
        print("Team with smallest differnce for and against goals is '{}', with {} \
goal difference.\n".format(self.output, self.minimum_difference))

