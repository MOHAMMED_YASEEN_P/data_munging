"""
Helper class module which includes all necessary classes.
"""
class DataExtractor:
    """Class which is used to extract data."""
    def __init__(self, path):
        """
        Initialized during object creation.

        :param self : either football or weather object.
        :type self : object.
        :param path : file path.
        :type path : string.
        """
        self.path = path

    def extract_data(self):
        """
        Method to extract data.

        :param self : data object.
        :type self : data object.
        """
        file_handler = open(self.path, 'r')
        data = file_handler.read().split("\n")
        self.length = len(data)
        self.data = data


class DataAnalyzer:
    """Class to analyze data."""

    def get_minimum_difference(self):
        """
        Method to analyze data.

        :param self : data object object.
        :type self : object.
        """
        for data in self.filtered_data:
            if self.minimum_difference is None or self.minimum_difference > abs(data[1]-data[2]):
                self.minimum_difference = abs(data[1]-data[2])
                self.output = data[0]

    def filter_data(self,extracted_data,column_to_be_extracted):
        """ filter data columns"""
        self.data = []
        for row_number,row in enumerate(extracted_data.data):
            if row_number == 0:
                continue
            row = row.split()
            if len(row) < 2:
                continue
            else :
                self.data.append([ row[column_to_be_extracted[0]], row[column_to_be_extracted[1]], row[column_to_be_extracted[2]] ])
