"""Weather module"""
from py_files import helper_class

class WeatherAnalyzer(helper_class.DataAnalyzer):
    """Weather class."""
    def __init__(self):
        self.minimum_difference = None
        self.output = None
    
    def filter_weather_data(self,extracted_data,column_to_be_extracted, excluded_lines):
        """
        Filter weather object.

        :param self : weather object.
        :type self : weather object.
        :param column_to_be_extracted : column which have requreid data.
        :type  column_to_be_extracted : list
        :param excluded_lines : unwanted rows.
        :type excluded_lines : list.
        """
        self.filtered_data = []
        self.filter_data(extracted_data,column_to_be_extracted)
        for row_number,row in enumerate(self.data):
            try:
                self.filtered_data.append([int(row[0]),int(row[1].replace("*","")),int(row[2].replace("*",""))])
            except:
                continue
    def get_result(self):
        print("\nsmallest temperature spread day is '{}', with a temperature\
spread of {}.\n".format(self.output, self.minimum_difference))
        print()


