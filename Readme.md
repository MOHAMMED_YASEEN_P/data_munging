# DATA MUNGING PROJECT

In this project we have two data files named weather.dat and football.dat.
The objective of this project is to learn refactoring according to SOLID principle.
This project is done in three parts.

### Part One: Weather Data

In weather.dat you’ll find daily weather data for Morristown, NJ for June 2002. We have to output the day number (column one) with the smallest temperature spread

### Part Two: Soccer League Table

The file football.dat contains the results from the English Premier League for 2001/2. The columns labeled ‘F’ and ‘A’ contain the total number of goals scored for and against each team in that season (so Arsenal scored 79 goals against opponents, and had 36 goals scored against them). We have to print the name of the team with the smallest difference in ‘for’ and ‘against’ goals.

### Part Three: DRY Fusion

Take the two programs written previously and factor out as much common code as possible, leaving you with two smaller programs and some kind of shared functionality.

## Technologies used :

- Python 3.7.4 64-bit (conda 4.8.2 environment)

## Instructions :

1) Run main.py using python 3.7 to get the output.

## DIRECTORY STRUCTURE :
- config.ini includes few controllers to control the flow of execution
    (warning : changes in config.ini can cause severe problems)
- data directory consists of data files needed for this project.
- main.py file consits of step wise met.hod calls.
- py_files directory consists of all python modules that are necessary for the execution of this project


        .
        ├── config.ini
        ├── data
        │   ├── football.dat
        │   └── weather.dat
        ├── main.py
        ├── py_files
        │   ├── football.py
        │   ├── helper_class.py
        │   ├── __init__.py
        │   └── weather.py
        └── Readme.md
